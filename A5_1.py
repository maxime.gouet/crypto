class A5_1:
#Constructeur de la classe A5_1
    def __init__(self,secret_key):
        self.register1 = int('0000000000000000000',2)
        self.register2 = int('0000000000000000000000',2)
        self.register3 = int('00000000000000000000000',2)
        self.compteur =  int('0000000000000000000000',2)
        self.key =  secret_key

#fonction d'affichage pour le debugg
    def printA5_1(self):
        print("Register 1: {}".format(format(self.register1, '019b')))
        print("Register 2: {}".format(format(self.register2, '022b')))
        print("Register 3: {}".format(format(self.register3, '023b')))

#Initialisation du des LFRS avec insertion de la clé et du compteur de bloc
    def Initialisation(self):
        mask1 = 2 ** 19 - 1
        mask2 = 2 ** 22 - 1
        mask3 = 2 ** 23 - 1

        for k in range(65):
            self.register1 = self.register1^((self.key &(1 << k)) >> k)
            self.register2 = self.register2^((self.key &(1 << k)) >> k)
            self.register3 = self.register3^((self.key &(1 << k)) >> k)

            tot = ((self.register1 &(1 << 8)) >> 8) +((self.register2 &(1 << 10)) >> 10) + ((self.register3 &(1 << 10)) >> 10)

            if tot >= 2:
                bitmajoritaire = 1
            else:
                bitmajoritaire = 0

            if ((self.register1 &(1 << 8)) >> 8) == bitmajoritaire:
                Xorresult = ((self.register1 &(1 << 18)) >> 18)^((self.register1 &(1 << 17)) >> 17)^((self.register1 &(1 << 16)) >> 16)^((self.register1 &(1 << 13)) >> 13)
                self.register1 = (self.register1 << 1)&mask1
                self.register1 = self.register1|Xorresult
            if ((self.register2 &(1 << 10)) >> 10) == bitmajoritaire:
                Xorresult = ((self.register2 &(1 << 20)) >> 20)^((self.register2 &(1 << 21)) >> 21)
                self.register2 = (self.register2 << 1)&mask2
                self.register2 = self.register2|Xorresult
            if ((self.register3 &(1 << 10)) >> 10) == bitmajoritaire:
                Xorresult = ((self.register3 &(1 << 22)) >> 22)^((self.register3 &(1 << 21)) >> 21)^((self.register3 &(1 << 20)) >> 20)^((self.register3 &(1 << 7)) >> 7)
                self.register3 = (self.register3 << 1)&mask3
                self.register3 = self.register3|Xorresult

        for k in range(23):
            self.register1 = self.register1^((self.compteur &(1 << k)) >> k)
            self.register2 = self.register2^((self.compteur &(1 << k)) >> k)
            self.register3 = self.register3^((self.compteur &(1 << k)) >> k)

            tot = ((self.register1 &(1 << 8)) >> 8) +((self.register2 &(1 << 10)) >> 10) + ((self.register3 &(1 << 10)) >> 10)

            if tot >= 2:
                bitmajoritaire = 1
            else:
                bitmajoritaire = 0

            if ((self.register1 &(1 << 8)) >> 8) == bitmajoritaire:
                Xorresult = ((self.register1 &(1 << 18)) >> 18)^((self.register1 &(1 << 17)) >> 17)^((self.register1 &(1 << 16)) >> 16)^((self.register1 &(1 << 13)) >> 13)
                self.register1 = (self.register1 << 1)&mask1
                self.register1 = self.register1|Xorresult
            if ((self.register2 &(1 << 10)) >> 10) == bitmajoritaire:
                Xorresult = ((self.register2 &(1 << 20)) >> 20)^((self.register2 &(1 << 21)) >> 21)
                self.register2 = (self.register2 << 1)&mask2
                self.register2 = self.register2|Xorresult
            if ((self.register3 &(1 << 10)) >> 10) == bitmajoritaire:
                Xorresult = ((self.register3 &(1 << 22)) >> 22)^((self.register3 &(1 << 21)) >> 21)^((self.register3 &(1 << 20)) >> 20)^((self.register3 &(1 << 7)) >> 7)
                self.register3 = (self.register3 << 1)&mask3
                self.register3 = self.register3|Xorresult

#Fonction pour récuperer 8 bit de sortie de A5/1 pour chiffrer le message , incrément en suite le compteur
    def Get8Bit(self):
        mask1 = 2 ** 19 - 1
        mask2 = 2 ** 22 - 1
        mask3 = 2 ** 23 - 1
        mask4 = 2 ** 8 - 1
        result = int('00000000',2)
        Xorresult = int('0',2)
        for i in range(9):

            tot = ((self.register1 &(1 << 8)) >> 8) +((self.register2 &(1 << 10)) >> 10) + ((self.register3 &(1 << 10)) >> 10)
            if tot >= 2:
                bitmajoritaire = 1
            else:
                bitmajoritaire = 0

            if ((self.register1 &(1 << 8)) >> 8) == bitmajoritaire:
                Xorresult = ((self.register1 &(1 << 18)) >> 18)^((self.register1 &(1 << 17)) >> 17)^((self.register1 &(1 << 16)) >> 16)^((self.register1 &(1 << 13)) >> 13)
                self.register1 = (self.register1 << 1)&mask1
                self.register1 = self.register1|Xorresult
            if ((self.register2 &(1 << 10)) >> 10) == bitmajoritaire:
                Xorresult = ((self.register2 &(1 << 20)) >> 20)^((self.register2 &(1 << 21)) >> 21)
                self.register2 = (self.register2 << 1)&mask2
                self.register2 = self.register2|Xorresult
            if ((self.register3 &(1 << 10)) >> 10) == bitmajoritaire:
                Xorresult = ((self.register3 &(1 << 22)) >> 22)^((self.register3 &(1 << 21)) >> 21)^((self.register3 &(1 << 20)) >> 20)^((self.register3 &(1 << 7)) >> 7)
                self.register3 = (self.register3 << 1)&mask3
                self.register3 = self.register3|Xorresult

            result = (result << 1)&mask4
            Xorresult = ((self.register3 &(1 << 22)) >> 22)^((self.register2 &(1 << 21)) >> 21)^((self.register1 &(1 << 18)) >> 18)
            result = result | Xorresult
        self.compteur += 1
        return result

#Algorithme de DiffieHellman qui permet de genrer une clé a partir d'un nombre P ( premier ) et une base G
def DiffieHellman():
    p = 7335465888343862968130285288842581941479997730776454051901203329440508367992945303835458008138016530970254318172965357391199557120543758438497838095839321
    g = 3
    secret_A = 80
    secret_B = 105
    A = (3^secret_A) % p
    B = (3^secret_B) % p

    secret_key_A = (A^secret_B) % p
    secret_key_B = (B^secret_A) % p
    if secret_key_A == secret_key_B:
        print(secret_key_A == secret_key_B)

    return secret_key_B

#Je n'uilise pas miller rabin par default pour des questions de rapidité, il est néanmoins ici si vous voulez le tester
def miller_rabin(n,d):
    if n == 2:
        return True

    if n % 2 == 0:
        return False

    r, s = 0, n - 1

    while s % 2 == 0:
        r += 1
        s //= 2

    for _ in xrange(d):
        a = randint(2, n - 1)
        x = pow(a, s, n)

        if x == 1 or x == n - 1:
            continue

        for _ in xrange(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False

    return True

def RunA5_1():
    message = input ("Enter your message: ")
    secret_key = DiffieHellman()
    print("key :",secret_key)
    algo = A5_1(secret_key)
    chiffrer = 0
    message_chiffrer = 0
    algo.Initialisation()
    for char in message:
        print(char)
        algo.printA5_1()
        result = algo.Get8Bit()
        chiffrer = (result^ord(char))
        print("8 bit de sortie du A5/1 :",hex(result))
        print("Lettre ",char," soit ",hex(ord(char))," xor ",hex(result)," donne : ",hex(chiffrer))
        algo.Initialisation()
        message_chiffrer = message_chiffrer << 8
        message_chiffrer = message_chiffrer|chiffrer
    print("Message chiffrer : ",hex(message_chiffrer))

RunA5_1()
