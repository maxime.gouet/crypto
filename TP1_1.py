# -*- coding: utf-8 -*-
# TP1 -1 Gouet Maxime
from Crypto.Hash import SHA256
import time



m = SHA256.new(data = b"lucas")
cpt = 1
nbitem = len(m.hexdigest())

t0 = time.perf_counter()

while m.hexdigest()[-6:] != "4c4f4c" :
    cpt += 1
    m.update(b"lucas")

t1 = time.perf_counter()
print("nb concatenation = ",cpt)
print("il a fallut ",t1-t0, "secondes")

m = SHA256.new(data = 5577796*b"lucas")
print(m.digest())
