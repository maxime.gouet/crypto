# -*- coding: utf-8 -*-
# TP1 -2 Gouet Maxime
#Les tests sont effectuer dans la fonction RunVerif qui effectue les test de bon fonctionnement des fonctions
import math
from Crypto.Hash import SHA256
from Crypto.Util import number
from random import *
from decimal import *

def pgcd(a,b):
    if b==0:
        return a
    else:
        r=a%b
        return pgcd(b,r)

def generer_cle_RSA(n):
    p = number.getStrongPrime(n)
    q = number.getStrongPrime(n)
    N = p*q
    e = number.getStrongPrime(n)
    phin = (p-1)*(q-1)
    if pgcd(e,phin) == 1:
        print("e et Phin sont bien premier entre eux")
        d = number.inverse(e,phin)
        dp = d%(p-1)
        dq = d%(q-1)
        ip = number.inverse(p,q)
        iq = number.inverse(q,p)
        return int(p),int(q),int(d),int(dp),int(dq),int(ip),int(iq),int(N)
    else:
        print("e et Phin ne sont pas premier entre eux")
        return False

def signature_RSA_CRT(m,sk):
    if m >= sk[7] :
        return False
    p = sk[0]
    q = sk[1]
    sp = pow(m,sk[3],p)
    sq = pow(m,sk[4],q)

    s1 = (sk[6]*q*sp+sk[5]*p*sq)%sk[7]

    s2 = pow(m,sk[2],sk[7])

    print("s1 ",s1)
    print("s2 ",s2)

    if s1 == s2:
        return True
    else:
        return False

def signature_RSA_CRT_faute(m,sK):
    if m >= sk[7] :
        return False

    p = sk[0]
    q = sk[1]
    sp = pow(m,sk[3],p)
    sq = pow(m,sk[4],q)+1

    s1 = ((sk[6]*(q*sp))+(sk[5]*(p*sq)))%sk[7]

    s2 = pow(m,sk[2],sk[7])

#def RSA_CRT_Bellcore(m, sK, pK):




def RunVerif():
    result = generer_cle_RSA(1024)
    if result:
        print(signature_RSA_CRT(number.bytes_to_long(b"test"),result))


RunVerif()
