# -*- coding: utf-8 -*-
# TP1 -2 Gouet Maxime
#Les tests sont effectuer dans la fonction RunVerif qui effectue les test de bon fonctionnement des fonctions
import math
from Crypto.Hash import SHA256
from Crypto.Util import number
from random import *

class Point:
    X = 0
    Y = 0
    Z = 0

    def __init__(self,PX,PY,PZ):
        self.X = PX
        self.Y = PY
        self.Y = PY
    def equal(self,other):
        return self.X == other.X and self.Y == other.Y

def addition_point(A,B,p,PP,PQ):
    if (PP.X == 0 and PP.Y == 0 and PP.Z == 0):
        return PQ
    if (PQ.X == 0 and PQ.Y == 0 and PQ.Z == 0):
        return PP
    if (not(PP.equal(PQ)) and PP.X == PQ.X) :
        return Point(0,0,0)

    if not(PP.equal(PQ)) and PP.X != PQ.X :
        lmbda = (PQ.Y-PP.Y)*number.inverse((PQ.X-PP.X),p)
        X = pow(lmbda,2)-PP.X-PQ.X
        Y = lmbda*(PP.X-X)-PP.Y
        return Point(X%p,Y%p,0)

    if PP.equal(PQ) and PP.Y == 0:
        return Point(0,0,0)

    if PP.equal(PQ) and PP.Y != 0:
        lmbda = (3*pow(PP.X,2)+A)*number.inverse(2*PP.Y,p)
        X = pow(lmbda,2)-2*PP.X
        Y = lmbda*(PP.X-X)-PP.Y
        return Point(X%p,Y%p,0)

def double_and_add(A,B,p,P,k):
    Q = Point(0,0,0)
    if k == 0:
        return Q

    n = int(math.log(k, 2)) + 1
    for i in range(n,-1,-1):
        Q = addition_point(A,B,p,Q,Q)
        if ((k>>i) & 1) == 1 :
            Q = addition_point(A,B,p,Q,P)
    return Q

def test_Hasse(n,p):
    print("1",p+1-int(2*math.sqrt(p)))
    print("2",p+1+int(2*math.sqrt(p)))
    return p+1-2*int(math.sqrt(p))<n<p+1+int(2*math.sqrt(p))

def ecdh(A,B,p,n,P):
    a = randint(1,n-1)
    b = randint(1,n-1)

    pts1 = double_and_add(A,B,p,P,b)
    pts2 = double_and_add(A,B,p,P,a)
    PA = double_and_add(A,B,p,pts1,a)
    PB = double_and_add(A,B,p,pts2,b)

    if not(PA.equal(PB)):
        return False
    else :
        x = SHA256.new()
        x.update(number.long_to_bytes(P.X))
        return x.hexdigest()

#La fonction ecdsa est l'application de l'algorithme du cours pour generer une signature
#la fonction apelle directement ecdsa_verif
def ecdsa(A,B,p,P,n,m,a):
    k = randint(1,n-1)
    K = double_and_add(A,B,p,P,k)
    t = K.X

    x = SHA256.new()
    x.update(m)
    m = x.hexdigest()
    m = int(m,16)

    i = int(ecdh(A,B,p,n,P),16)
    s = ((m+i*t)*number.inverse(k,n))%n

    ecdsa_verif(A,B,p,P,n,m,i,t,s)

    return (t,s)

#la fonction ecdsa_verif est l'application de l'algorithme du cours pour verifier la signature produite pas ecdsa
def ecdsa_verif(A,B,p,P,n,m,A1,t,s):
    Q1 = double_and_add(A,B,p,P,(m*number.inverse(s,n)))

    H = double_and_add(A,B,p,P,A1)
    Q2 = double_and_add(A,B,p,H,(t*number.inverse(s,n)))

    Q = addition_point(A,B,p,Q1,Q2)
    if Q.X%n == t:
        print("La signature est Valider")
    else :
        print("La signatuer est Non Valider")


def RunVerif():
    p = 115792089210356248762697446949407573530086143415290314195533631308867097853951
    n = 115792089210356248762697446949407573529996955224135760342422259061068512044369
    B = int('5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b',16)
    Gx= int('6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296',16)
    Gy= int('4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5',16)

    G = Point(Gx,Gy,0)

#Les valuer de born inférieur et supérieur sont identique aux premier abords , mais c'est simplement car python a simplifier, si on regarde la valeur réelle on constate qu'elle est différente
    print("Hasse ",test_Hasse(n,p))
    print("ecdh ",ecdh(-3,B,p,n,G))
    print("ecdsa ",ecdsa(-3,B,p,G,n,b"Test",6))

RunVerif()
