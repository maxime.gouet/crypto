# -*- coding: utf-8 -*-
# TP1 -2 Gouet Maxime
#Les tests sont effectuer dans la fonction RunVerif qui effectue les test de bon fonctionnement des fonctions
from Crypto.Util import number
import math

class Point:
    X = 0
    Y = 0
    Z = 0

    def __init__(self,PX,PY,PZ):
        self.X = PX
        self.Y = PY
        self.Y = PY
    def equal(self,other):
        return self.X == other.X and self.Y == other.Y

def verifie_point(A,B,p,P):
    if P.X == 0 and P.Y == 0 and P.Z == 0:
        return True
    else:
        return pow(P.Y,2)%p == (pow(P.X,3)+A*P.X+B)%p

def addition_point(A,B,p,PP,PQ):
    if (PP.X == 0 and PP.Y == 0 and PP.Z == 0):
        return PQ
    if (PQ.X == 0 and PQ.Y == 0 and PQ.Z == 0):
        return PP
    if (not(PP.equal(PQ)) and PP.X == PQ.X) :
        return Point(0,0,0)

    if not(PP.equal(PQ)) and PP.X != PQ.X :
        lmbda = (PQ.Y-PP.Y)*number.inverse((PQ.X-PP.X),p)
        X = pow(lmbda,2)-PP.X-PQ.X
        Y = lmbda*(PP.X-X)-PP.Y
        return Point(X%p,Y%p,0)

    if PP.equal(PQ) and PP.Y == 0:
        return Point(0,0,0)

    if PP.equal(PQ) and PP.Y != 0:
        lmbda = (3*pow(PP.X,2)+A)*number.inverse(2*PP.Y,p)
        X = pow(lmbda,2)-2*PP.X
        Y = lmbda*(PP.X-X)-PP.Y
        return Point(X%p,Y%p,0)

def groupe_des_points(A,B,p):
    list = []
    for X in range(p) :
        for Y in range(p):
            if verifie_point(A,B,p,Point(X,Y,0)):
                list.append(Point(X,Y,0))
    return list

def ordre_point(A,B,p,P):
    X = Point(P.X,P.Y,P.Z)
    c = 1
    while X.X != 0 and X.Y != 0 :
        X = addition_point(A,B,p,X,P)
        c += 1
    return c

def generateurs(A,B,p):
    groupe = groupe_des_points(A,B,p)
    list = []
    for i in groupe:
        if(ordre_point(A,B,p,i) == len(groupe)):
            list.append(i)
    return list

def double_and_add(A,B,p,P,k):
    Q = Point(0,0,0)
    if k == 0:
        return Q

    n = int(math.log(k, 2)) + 1
    for i in range(n,-1,-1):
        Q = addition_point(A,B,p,Q,Q)
        if ((k>>i) & 1) == 1 :
            Q = addition_point(A,B,p,Q,P)
    return Q


#Fonctione de test
def RunVerif():
    pts1 = Point(2,1,0)
    pts2 = Point(2,2,0)
    pts3 = Point(0,0,0)
    pts4 = Point(2,4,0)
    pts5 = Point(1,1,0)
    pts6 = Point(1,4,0)

    print("0infini:E1 :",verifie_point(3,2,5,pts3))
    print("2,1:E1 :",verifie_point(3,2,5,pts1))
    print("2,2:E1 :",verifie_point(3,2,5,pts2))

    pts7 = addition_point(3,2,5,pts1,pts4)
    pts8 = addition_point(3,2,5,pts1,pts1)
    pts9 = addition_point(3,2,5,pts1,pts3)
    pts10 = addition_point(3,2,5,pts1,pts5)
    pts11 = addition_point(3,2,5,pts1,pts6)
    pts12 = addition_point(3,2,5,pts6,pts6)

    print("2,1+2,4 X:",pts7.X," Y : ",pts7.Y)
    print("2,1+2,1 :",pts8.X," Y : ",pts8.Y)
    print("2,1+0,0 :",pts9.X," Y : ",pts9.Y)
    print("2,1+1,1 :",pts10.X," Y : ",pts10.Y)
    print("2,1+1,4 :",pts11.X," Y : ",pts11.Y)
    print("1,4+1,4 :",pts12.X," Y : ",pts12.Y)

    list = groupe_des_points(1,2,3)
    for i in list:
        print("Pour E1 P.X : ",i.X," P.Y : ",i.Y)

    print(len(list))

    list2 = groupe_des_points(1,2,11)
    for i in list2:
        print("Pour E2 P.X : ",i.X," P.Y : ",i.Y)

    print(len(list2))

    #donc E1 est effectivement cyclique puisque tous ses points sont générateur
    list3 = generateurs(3,2,5)
    print("generateur E1 ",len(list3))

    #donc E2 n'est effectivement pas cyclique
    list4= generateurs(1,2,11)
    print("generateur E2 ",len(list4))

    pts13 = double_and_add(3,2,5,pts4,0)
    pts14 = double_and_add(3,2,5,pts4,1)
    pts15 = double_and_add(3,2,5,pts4,2)
    pts16 = double_and_add(3,2,5,pts4,3)
    pts17 = double_and_add(3,2,5,pts4,4)
    pts18 = double_and_add(3,2,5,pts4,5)

    print("double and add k=1 X:",pts13.X," Y : ",pts13.Y)
    print("double and add k=1 X:",pts14.X," Y : ",pts14.Y)
    print("double and add k=2 X:",pts15.X," Y : ",pts15.Y)
    print("double and add k=3 X:",pts16.X," Y : ",pts16.Y)
    print("double and add k=4 X:",pts17.X," Y : ",pts17.Y)
    print("double and add k=5 X:",pts18.X," Y : ",pts18.Y)


RunVerif()
