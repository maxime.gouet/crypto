# -*- coding: utf-8 -*-
# TP1 -2 Gouet Maxime
from Crypto.Cipher import AES
from Crypto.Util import number
from Crypto.Util import number
import time
import Crypto

# L'espace de la clé dans le cas ILOVEYOU, ILOVEYOU etant codé sur 64 bit il reste 64 bit pour la clé soit : 2^128-64 possiblité
# L'espace de la clé dans le cas LOL, LOL etant codé sur 24 bits il reste 104 pour la clé bit soit: 2^128-24 possiblité

key = Crypto.Random.get_random_bytes(16)
message = 16*b'x'
chiffrement = AES.new(key, AES.MODE_ECB)
m_chiffre   = chiffrement.encrypt(message)
cpt = 1

t0 = time.perf_counter()

while hex(number.bytes_to_long(m_chiffre))[-6:] != "4c4f4c":
    cpt += 1
    intkey = number.bytes_to_long(key)+1
    key = number.long_to_bytes(intkey)
    chiffrement = AES.new(key, AES.MODE_ECB)
    m_chiffre   = chiffrement.encrypt(message)

t1 = time.perf_counter()
print("il a fallut ",t1-t0, "secondes")
print(m_chiffre)
